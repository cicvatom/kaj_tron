import { Game } from "./game.js";

// Menu logic
// 'Start' and 'Leaderboard' button click 
const gameButton = document.querySelector('#game-button');
const leadButton = document.querySelector('#lead-button');
gameButton.addEventListener('click', () => {
    const gameBoard = document.getElementById('game');
    const menu = document.getElementById('menu');
    menu.style.display = 'none';
    gameBoard.style.display = 'block';
    const canvas = document.querySelector('canvas');
    const ctx = canvas.getContext('2d');
    let game = new Game(canvas, ctx);
});

leadButton.addEventListener('click', () => {
    const leadBoard = document.getElementById('leaderboard');
    const menu = document.getElementById('menu');
    menu.style.display = 'none';
    leadBoard.style.display = 'flex';

    let ulNames = document.getElementById('names');
    ulNames.innerHTML = '';
    let ulScores = document.getElementById('scores');
    ulScores.innerHTML = ''

    console.log(localStorage);
    for(let i = 0; i < localStorage.length; i++){
        console.log(i, localStorage.key(i));
        let key = localStorage.key(i);
        let liN = document.createElement("li");
        let liS = document.createElement("li");
        liN.appendChild(document.createTextNode(key));
        liS.appendChild(document.createTextNode(localStorage[key]));
        ulNames.appendChild(liN);
        ulScores.appendChild(liS);
    }
});

// Leaderboard logic
// 'Back' button behavior, simulateous scroll

const backButton = document.querySelector('#back');
backButton.addEventListener('click', () => {
    const leadBoard = document.getElementById('leaderboard');
    const menu = document.getElementById('menu');
    menu.style.display = 'flex';
    leadBoard.style.display = 'none';
});

const names =  document.getElementById('names');
const scores =  document.getElementById('scores');
names.addEventListener('scroll', () => { 
    scores.scrollTop = names.scrollTop;
}, false);
scores.scroll(() => { 
    names.scrollTop = scores.scrollTop();
}, false);



// Modal window logic
// Listening to game end, clicking 'Close button'

let modalVisible = false;
const modalButtons = document.querySelectorAll('.modal-button');

for (let i = 0; i < modalButtons.length; i++) {
    modalButtons[i].addEventListener('click', toggleModalState2);
}
document.addEventListener('GameFin', e => toggleModalState(e));

/**
 * Function called by the game. Opens modal window to save results.
 * @param {*} e game finish event
 */
function toggleModalState (e)
{
    document.getElementById('name-input').value = '';
    
    console.log(e.detail);
    if(e.detail == 0){
        let content = document.getElementById('result');
        content.innerHTML = 'Tie';
        document.getElementById('name-input').style.visibility = 'hidden';
    }
    else{
        let content = document.getElementById('result');
        content.innerHTML = 'Winner';
        document.getElementById('name-input').style.visibility = 'visible';
    }
    
    modalVisible = !modalVisible;
    if (modalVisible) {
    document.body.classList.add('modal-visible');
    }
    else {
        document.body.classList.remove('modal-visible');
    }
}

/**
 * Function toggled by button in modal window. Saves the resulting name and switches to main menu.
 */
function toggleModalState2 ()
{
    let nameCopy = document.getElementById('name-input').value.slice(0);
    const gameBoard = document.getElementById('game');
    const menu = document.getElementById('menu');
    menu.style.display = 'flex';
    gameBoard.style.display = 'none';
    console.log(nameCopy);
    
    modalVisible = !modalVisible;
    if (modalVisible) {
    document.body.classList.add('modal-visible');
    }
    else {
        document.body.classList.remove('modal-visible');
    }
    document.getElementById('name-input').style.visibility = 'hidden';

    if(nameCopy.length == 0){
        return;
    }
    if(localStorage[nameCopy]){
        localStorage[nameCopy] = (parseInt(localStorage[nameCopy]) + 1).toString();
    }
    else{
        localStorage[nameCopy] = '1';
    }
}

