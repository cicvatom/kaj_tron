import { Player } from "./player.js";

export class Game{
    /**
     * Constructor prepares necessary variables and events and enables the start of the game.
     * @param {*} canvas canvas
     * @param {*} ctx canvas content
     */
    constructor(canvas, ctx)
    {
        this.canvas = canvas;
        this.ctx = ctx;
        this.player1 = new Player('media/rider01.png',
                                    ['w','s', 'a', 'd'], this.ctx, [0, 1], [516, 64]);
        this.player2 = new Player('media/rider02.png',
                                    ['ArrowUp','ArrowDown', 'ArrowLeft', 'ArrowRight'], this.ctx, [0, -1], [516, 936]);
        this.background = new Image();
        this.background.src = 'media/tron_grid.png';
        this.time = 0;
        this.calls = 4;
        this.ctx.font = '72px serif';
        this.ctx.textAlign = "center";
        this.speed = 0.07;
        this.ended = false;
        this.winner = -1;

        this.prepareGame = this.prepareGame.bind(this);
        this.play = this.play.bind(this);
        this.keyboard = this.handleKeyboard.bind(this);


        document.addEventListener('keydown', e => this.handleKeyboard(e), true);

        this.background.addEventListener('load', () => {
            requestAnimationFrame(this.prepareGame);
        });
    }

    /**
     * Draws background to the canvas.
     */
    drawBackground()
    {
        this.ctx.drawImage(this.background, 0, 0);
    }

    /**
     * Method used at the beginning for countdown.
     * @param {*} text text to be drawn
     */
    drawText(text)
    {
        this.ctx.fillStyle = 'red';
        this.ctx.fillText(text, this.canvas.width/2, this.canvas.height/2);
        this.ctx.fillStyle = 'violet';
        this.ctx.strokeText(text, this.canvas.width/2, this.canvas.height/2);
        this.ctx.fillStyle = 'red';
    }

    /**
     * Prepares the game, starts countdown and starts the game.
     * @param {*} timestamp time when the method is called
     */
    prepareGame(timestamp)
    {
        if(this.calls != 4 && timestamp - this.time < 1000){
            requestAnimationFrame(this.prepareGame);
            return;
        }
        this.calls -= 1;
        if (this.calls > 0){
            this.time = timestamp;
            this.drawBackground();
            this.player1.draw();
            this.player2.draw();
            this.drawText(this.calls.toString());
        }
        else if (this.calls == 0){
            this.time = timestamp;
            this.drawBackground();
            this.player1.draw();
            this.player2.draw();
            this.drawText('start')
        }
        else{
            this.time = timestamp;
            requestAnimationFrame(this.play);
            return;
        }
        requestAnimationFrame(this.prepareGame);
    }

    /**
     * Continuously called method while the game is running.
     * @param {*} timestamp time when the method is called
     */
    play(timestamp){
        const dt = timestamp - this.time;
        if(! this.ended){
            let w1;
            let w2;
            let h1;
            let h2;
            if(this.player1.direction[0] == 0){
                w1 = this.player1.image.width;
                h1 = this.player1.image.height;
            }
            else{
                w1 = this.player1.image.height;
                h1 = this.player1.image.width;
            }
            if(this.player2.direction[0] == 0){
                w2 = this.player2.image.width;
                h2 = this.player2.image.height;
            }
            else{
                w2 = this.player2.image.height;
                h2 = this.player2.image.width;
            }
            if(Math.abs(this.player1.position[0] - this.player2.position[0]) < Math.abs(w1 / 2 + w2 / 2) &&
            Math.abs(this.player1.position[1] - this.player2.position[1]) < Math.abs(h1 / 2 + h2 / 2)){
                this.winner = 0;
                this.ended = true;
            }
        
            if(! this.player1.lineCollision()){
                this.ended = true;
                if(this.winner == 1){
                    this.winner = 0;
                }
                else if (this.winner < 0){
                    this.winner = 2;
                }
            }
            if(!this.player2.lineCollision()){
                this.ended = true;
                if(this.winner == 2){
                    this.winner = 0;
                }
                else if (this.winner < 0){
                    this.winner = 1;
                }
            }
            if(!this.player1.update([48, 952, 48, 952], dt, this.speed)){
                this.ended = true;
                if(this.winner == 1){
                    this.winner = 0;
                }
                else if (this.winner < 0){
                    this.winner =2;
                }
            }
            if(!this.player2.update([48, 952, 48, 952], dt, this.speed)){
                this.ended = true;
                if(this.winner == 2){
                    this.winner = 0;
                }
                else if (this.winner < 0){
                    this.winner = 1;
                }
            }
            if(this.ended){
                console.log('event dispatch');
                let event = new Event('GameFin');
                event.detail = this.winner;
                document.dispatchEvent(event);
                return;
            }
        }
        this.drawBackground();
        this.player1.drawPath();
        this.player2.drawPath();
        this.player1.draw();
        this.player2.draw();
        
        this.time = timestamp;
        requestAnimationFrame(this.play);
    }

    /**
     * Keyboard event handler.
     * @param {} e keyboard event
     */
    handleKeyboard(e){
        console.log('clicking');
        if(this.calls > 0 || this.ended){
            return;
        }
        if(e.key == 'ArrowUp'){
            this.player1.changeDirection([0, -1]);
            e.preventDefault();
        }
        else if(e.key == 'ArrowDown'){
            this.player1.changeDirection([0, 1]);
            e.preventDefault();
        }
        else if(e.key == 'ArrowLeft'){
            this.player1.changeDirection([-1, 0]);
            e.preventDefault();
        }
        else if(e.key == 'ArrowRight'){
            this.player1.changeDirection([1, 0]);
            e.preventDefault();
        }

        else if(e.key == 'w'){
            this.player2.changeDirection([0, -1]);
            e.preventDefault();
        }
        else if(e.key == 's'){
            this.player2.changeDirection([0, 1]);
            e.preventDefault();
        }
        else if(e.key == 'a'){
            this.player2.changeDirection([-1, 0]);
            e.preventDefault();
        }
        else if(e.key == 'd'){
            this.player2.changeDirection([1, 0]);
            e.preventDefault();
        }
    }    
}
